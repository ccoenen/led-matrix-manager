const helpers = {
	outputbuffer: [],

	stripNonPrintables: function (buffer) {
		return Array.from(buffer.values())
			.map(Number)
			.filter(v => { return v >= 0x0A && v <= 0x7F })
			.map(c => String.fromCharCode(c))
			.join("");
	},

	redirectOutput: function (io) {
		io.on("data", (data) => {
			helpers.outputbuffer.push(data);
			console.log(helpers.stripNonPrintables(data));
		});
	}
};

module.exports = helpers;
